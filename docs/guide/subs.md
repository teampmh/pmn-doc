# 订阅链接
::: tip 提示
Paimon Node 1.5已于2023年8月22日释出。
:::

**Base64(通用)：**
```https://sub.pmsub.me/base64```

**Clash：**
```https://sub.pmsub.me/clash.yaml```

欢迎订阅我们的Telegram频道！[点击加入](https://t.me/nodpai/)
