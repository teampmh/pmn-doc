---
layout: home
hero:
  name: Paimonnode
  text: 所有人的免费订阅.
  tagline: 享受极佳的网络浏览体验
  image:
    src: /logo.png
    alt: Logo
  actions:
    - theme: brand
      text: 立即订阅！
      link: /guide/subs
    - theme: brand
      text: 我是新手？从0开始
      link: /new-guide/index
features:
  - icon: ⚡️
    title: 高速浏览
    details: 精选高速节点，体验无差别浏览
  - icon: ⏲
    title: 每日更新
    details: 自动爬取筛选节点，保持高频率更新
  - icon: 👍
    title: 更佳质量
    details: 自研筛选引擎，节点更可靠
---
