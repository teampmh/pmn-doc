# 新手？那让我们开始吧！

::: warning
新手文档还在施工中，请以最终效果为准
:::


| 系统              | 客户端                                                       |
| ----------------- | ------------------------------------------------------------ |
| Android/HarmonyOS | [ClashForAndroid](https://github.com/Kr328/ClashForAndroid/releases) [SagerNet](https://github.com/SagerNet/SagerNet/releases) [v2rayNG](https://github.com/2dust/v2rayNG/releases) |
| Windows           | [Clash_for_Windows](https://github.com/Fndroid/clash_for_windows_pkg/releases) [v2rayN](https://github.com/2dust/v2rayN/releases) |
| iOS               | [Shadowrocket](https://apps.apple.com/us/app/shadowrocket/id932747118) [Stash](https://apps.apple.com/us/app/stash/id1596063349) |
| macOS             | [Clash_for_windows](https://github.com/Fndroid/clash_for_windows_pkg/releases) [ClashX](https://github.com/yichengchen/clashX) |
| Linux             | [Clash_for_Windows](https://github.com/Fndroid/clash_for_windows_pkg/releases) [v2rayA](https://github.com/v2rayA/v2rayA) |

根据你的系统选择你的客户端。当然，你也可以使用你喜欢的客户端。

## 使用Clash系客户端

#### 方法1.下载好客户端后，你可以 **[点击此处](clash://install-config?url=https://sub.pmsub.me/clash.yaml)** 尝试一键导入配置文件。

::: warning 注意
此方式添加的Clash配置文件没有开启自动更新，你需要在客户端内手动开启它。
:::


#### 方法2.手动添加

- ##### 使用 Clash for Windows

下载并打开Clash for Windows客户端，切换到`Profiles`选项卡，在`Download from a URL`栏中填入订阅链接，然后点击`Download`按钮即可完成配置文件的添加。添加好配置后，点击新出现的`clash.yaml`文件，即可切换到PaimonNode的服务。切换到服务后，回到`General`选项卡，打开`System Proxy`，这样就可以自动配置系统代理了。

::: tip 注意
①：此方式添加的Clash配置文件**没有开启自动更新**，你需要右键点击`clash.yaml`，选择`Setting`，在`Update Interval (hour)`栏中设置自动更新的间隔时间。

②：你可以使用 [Clash_Chinese_Patch](https://github.com/BoyceLig/Clash_Chinese_Patch) [Clash-for-Windows_Chinese](https://github.com/ender-zhao/Clash-for-Windows_Chinese) 来将程序修改为中文，具体方法请参考项目的README.md文件，请注意，这些项目并非由我们提供，因此我们无法对其安全性提供保证。在使用任何第三方软件或工具时，请务必自行评估其安全性。

③：部分系统可能无法自动设定系统代理，请在你的系统设置中手动设置代理为`all_proxy=socks5://127.0.0.1:7890` `https_proxy=http://127.0.0.1:7890`  `http_proxy=http://127.0.0.1:7890` 
:::

![cfw](https://gitlab.com/teampmh/pmn-doc/-/raw/main/docs/new-guide/assets/cfw.png?ref_type=heads)

- ##### 使用 Clash for Android

![cfa](https://gitlab.com/teampmh/pmn-doc/-/raw/main/docs/new-guide/assets/cfa.png?ref_type=heads)

根据上面的图片指引操作即可。在第五步时可能会要求输入手机密码。
