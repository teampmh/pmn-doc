# Clash for windows高危漏洞
## 提示

Clash for windows 是 Clash 在Windows上的一个客户端之一，用户多
Clash Premium 是 Clash 的一个闭源分支，提供了很多实用功能。
Clash是一个代理软件。

## 漏洞发生原因：

### Part 1. 规则集

Clash Premium有个玩意叫 rule-providers ，使用来提供规则集的。

*什么是规则？*

你用的各种订阅里都有规则。比如：

![规则](https://gitlab.com/teampmh/pmn-doc/-/raw/main/docs/others/assets/rule.PNG?ref_type=heads)

你可以使用它，规定哪些站点需要用节点，或者用哪个节点……

这样，帮你选择节点以及分流策略的东西就叫规则。

规则集这个东西，说简单点就是给你实时提供这样各种规则，各种大佬的规则任你所用

**这个东西在使用的时候需要把规则缓存到本地**，这点很重要，这个要考

### Part 2. 配置文件

是这样的，ClashForWindows有一个配置文件，允许用户自定义自己的配置文件解析器

CFW每次解析的时候都会看看配置文件里有没有自定义的解析器，没有就自己干活。

如果有，就运行用户自定义的解析器，**解析器是由一段代码控制的**

### Part 3. 天时地利人和

但是昨天突然有人提了这样一个想法：

我通过**规则集的缓存功能**，把一个恶意的配置文件替换成**ClashForWindows的配置文件**，让他每一次更新订阅的时候都执行我的代码，我恶意攻击的目的就达到了

然后ClashForWindows这个傻白甜还真的傻傻的去执行所谓的“配置文件”的代码。。。

所以当你每次更新所有的订阅的时候，ClashForWindows就会执行一次解析器，也就是执行一次别人的代码

别人的代码可能包含挖矿啊钓鱼啊啥啥啥的玩意，到时候哭的不是CFW而是你了

## 漏洞影响范围：

可能是 **所有** 使用 Clash Premium 的客户端，因为只有 Clash Premium 有规则集（rule-providers）这个功能

CFW版本包括最新版本0.20.12及以下的所有版本

你也可以使用我们的检测脚本，具体检测方法如下：

## 检测脚本

检测脚本链接：https://sub.pmsub.me/cfw-rce-test/test-your-clash.yaml

首先，使用你的Clash客户端订阅我们的检测脚本并下载使用

然后，重启Clash客户端

最后，随便更新使用一个订阅

如果你的Clash客户端受此漏洞影响，那么你将会看到我们的提醒。

## 恢复脚本

恢复脚本链接：https://sub.pmsub.me/cfw-rce-test/reborn-your-clash.yaml

使用方法同上面的检测脚本

恢复之前请把检测脚本的规则删除。

## 建议

1. **建议仅更新可信的机场订阅**，你大可安心使用Paimon Node，我们有相关的安全防护保证此漏洞不会出现在Paimon Node中
2. 建议停止使用Clash客户端或者Clash Premium，使用其它客户端，如V2rayN等等
3. 如果非要使用clash premium，请使用core+sandbox panel(yacd之类)形式的方式运行


使用CC BY-NC-SA 4.0 进行许可。
Paimon Hub,
2023/1/15.
