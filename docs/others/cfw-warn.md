# 警告，您的电脑可能在遭受攻击！

Paimonnode安全系统发现了您正在使用具有高危安全漏洞的Clash for Windows版本，在此版本上，黑客可以远程在您的电脑上植入未经授权的代码并且静默运行！

您可以通过临时切换到以下软件来临时避免漏洞。

| 系统              | 客户端                                                       |
| ----------------- | ------------------------------------------------------------ |
| Windows           | [NekoRay](https://github.com/MatsuriDayo/nekoray) [v2rayN](https://github.com/2dust/v2rayN/releases) | 
| macOS             | [NekoRay](https://github.com/MatsuriDayo/nekoray) [ClashX](https://github.com/yichengchen/clashX) |
| Linux             | [NekoRay](https://github.com/MatsuriDayo/nekoray) [v2rayA](https://github.com/v2rayA/v2rayA) |


## 事件详细视频（来自不良林）

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/dnFu2Je20XE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## 我们关于此漏洞的介绍

[一键跳转](https://pmsub.me/others/cfw-read.md)