export default {
    title: 'Paimon Node',
    description: 'Free nodes for everyone.',
    themeConfig: {
        nav: [
            {
                text: '订阅链接',
                items: [
                    { text: 'Base64(通用)', link: 'https://sub.pmsub.me/base64' },
                    { text: 'Clash', link: 'https://sub.pmsub.me/clash.yaml' }
                ]
            }
        ],
        socialLinks: [
            { icon: 'github', link: 'https://github.com/paimonhub/Paimonnode' },
        ]
    },
    outDir: '../public'
}
